import RandomStats from "../../components/RandomStats";
import Header from "../../components/Header";
import Gamer from "../../components/Gamer";
import MyTest from "../../components/MyTest";
import './style.css';
import {
  UserStatsProvider,
  OponentStatsProvider,
  GameState,
} from "../../context";
import Form from "../../components/Form";
import { useContext, useEffect,useRef } from "react";

const Homepage = () => {
  const { isName, setName } = useContext(GameState);
  useEffect(() => {
    console.log(isName);
    const getName = localStorage.getItem("name");
    if (getName && getName.length > 0) {
      setName(true);
    }
  }, [isName]);

  const refPol=useRef(null);
  const refLas=useRef(null);
  const refDżu=useRef(null);
  const refPus=useRef(null);
  const refGór=useRef(null);
  return (
    <div>
      <div className="main-container">
        <Header{...{refPol,refLas,refDżu,refPus,refGór}}/>
        <MyTest/>
        <div className="col-12 destination" ref={refPol}><p>Polana</p></div>
        <div className="col-12 destination" ref={refLas}><p>Las</p></div>
        <div className="col-12 destination" ref={refDżu}><p>Dżungla</p></div>
        <div className="col-12 destination" ref={refPus}><p>Pustynia</p></div>
        <div className="col-12 destination" ref={refGór}><p>Góry</p></div>

      {isName ? (
        <div className="main-wrapper">
          <UserStatsProvider>
            <Gamer />
          </UserStatsProvider>

          <OponentStatsProvider>
            <RandomStats />
            <Gamer alive />
          </OponentStatsProvider>
        </div>
      ) : (
        <Form />
      )}</div>
    </div>

  
  );
};

export default Homepage;

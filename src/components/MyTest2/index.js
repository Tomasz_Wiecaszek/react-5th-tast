import MyMouse from "../MyTest/myMouse"
import MyTxt2 from "../MyTest2/MyTxt2"

const MyTest2 =()=>{
return(
    <div>
         <MyMouse render={
        mousePos=>{return (<MyTxt2 mousePos={mousePos}/>)}
        }/>
    </div>
)
}

export default MyTest2
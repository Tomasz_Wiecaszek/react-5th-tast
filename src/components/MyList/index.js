import React from "react";

const MyList=(props)=>{
    return(
        
            <li>Ulubiona potrawa: <strong>{props.food}</strong></li>
    )
}
export default MyList;

// class MyList extends React.Component{
//     render(){
//     return(
        
//             <li>Ulubiona potrawa: <strong>{this.props.food}</strong></li>
//     )}
// }
// export default MyList;
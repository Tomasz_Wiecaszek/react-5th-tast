import './style.css'
import TableRows from '../../components/TableRows'
import {CharStats} from "../../context"
import {useContext, useEffect} from 'react'


const User =()=>{
    let {name,str,hp,speed,dmg,addStr,addName,lvl}=useContext(CharStats);
    const myStats={name,str,hp,speed,dmg,lvl};
useEffect(()=>{
    addName(localStorage.getItem("name"));
},);
    return(
<div className="col-6">
    
    <TableRows {...myStats}/>
    
    <div onClick={()=>addStr(str+1)}>Add +1</div>
    </div>
    )}

export default User;
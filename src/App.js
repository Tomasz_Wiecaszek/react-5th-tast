
import './App.css';
import Homepage from './pages/homepage';
import {GameApp} from './context'

function App() {

  return (
    <div>
  <GameApp>
      <Homepage/>
  </GameApp>
    </div>
  );
}

export default App;
